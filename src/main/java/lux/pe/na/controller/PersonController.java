package lux.pe.na.controller;

import lombok.AllArgsConstructor;
import lux.pe.na.model.PersonDto;
import lux.pe.na.repository.PersonRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class PersonController {

  private final PersonRepository personRepository;

  @GetMapping("/people")
  public List<PersonDto> people() {
    return personRepository.findAllPeople();
  }
}
