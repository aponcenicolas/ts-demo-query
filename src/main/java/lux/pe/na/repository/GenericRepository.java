package lux.pe.na.repository;

import lux.pe.na.model.PersonDto;

import java.util.List;

public interface GenericRepository {

  List<PersonDto> findAllPeople();
}
