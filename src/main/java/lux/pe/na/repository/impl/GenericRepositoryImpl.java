package lux.pe.na.repository.impl;

import lux.pe.na.model.PersonDto;
import lux.pe.na.repository.GenericRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.util.List;
import java.util.stream.Collectors;

public class GenericRepositoryImpl implements GenericRepository {

  @PersistenceContext
  private EntityManager entityManager;


  @Override
  public List<PersonDto> findAllPeople() {
    Query query = entityManager.createNativeQuery("select p.name, p.age,pv.name as province from people p\n" +
        "inner join provinces pv on p.id_province = pv.id  ", Tuple.class);
    List<Tuple> people = query.getResultList();
    return people.stream().map(tuple -> PersonDto.builder()
        .name(tuple.get("name").toString())
        .age(Integer.parseInt(tuple.get("age").toString()))
        .province(tuple.get("province").toString())
        .build()
    ).collect(Collectors.toList());
  }
}
