package lux.pe.na.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "people")
public class Person implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "age")
  private Integer age;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_province")
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Province province;

  /*@Column(name = "id_province")
  private Long idProvince;*/

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "id_car")
  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
  private Car car;

  /*@Column(name = "id_car")
  private Long idCar;*/

}
