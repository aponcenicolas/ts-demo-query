package lux.pe.na.model;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private String name;
  private Integer age;
  private String province;
}
